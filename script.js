// Теоретический вопрос
//
// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
//
//
// Задание
// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.
//     Технические требования:
//     Создать функцию, которая будет принимать на вход массив.
//     Каждый из элементов массива вывести на страницу в виде пункта списка
// Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.
//     Примеры массивов, которые можно выводить на экран:
//     ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
//         ['1', '2', '3', 'sea', 'user', 23]
// Можно взять любой другой массив.
//     Необязательное задание продвинутой сложности:
//     Очистить страницу через 10 секунд. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.
//     Если внутри массива одним из элементов будет еще один массив или объект, выводить его как вложенный список.


let quantityList = parseInt(prompt("Enter number of countries ", ""));
let sec = 10;

let countryNames = [];
for (let i = 0; i < quantityList; i++) {
    countryNames[i] = prompt("Enter name of country ", "");
}

function showList(countries) {
    let countriesList = countries.map(item => `<li>${item}</li>`);
    document.getElementById('list').innerHTML = countriesList.join("\n");
    timeSec();
}

function timeSec() {
    if (sec < 1) {
        document.body.innerHTML = '';
    } else {
        document.getElementById('timer').innerHTML = sec;
        setTimeout(timeSec, 1000);
        sec--;
    }
}

showList(countryNames);